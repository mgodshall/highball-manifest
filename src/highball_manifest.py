#!/usr/bin/env python

#
# Generate job build scripts for highball from YAML specifications.
#
# Usage:
#


from cgi import escape
import argparse
import copy
import jenkins
import pystache
import pystache.defaults
import sys
import xml.etree.ElementTree as ET
from pprint import pprint
from yaml import load, dump, YAMLError
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

single_job_xml = """
<project>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <scm/>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <triggers/>
  <concurrentBuild>false</concurrentBuild>
  <builders/>
  <publishers/>
  <buildWrappers/>
</project>
"""

matrix_job_xml = """
<matrix-project plugin="matrix-project@1.3">
  <actions/>
  <description></description>
  <keepDependencies>false</keepDependencies>
  <properties/>
  <scm/>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <triggers/>
  <concurrentBuild>false</concurrentBuild>
  <axes>
    <hudson.matrix.TextAxis>
    </hudson.matrix.TextAxis>
  </axes>
  <builders/>
  <publishers>
  </publishers>
  <buildWrappers/>
  <executionStrategy class="hudson.matrix.DefaultMatrixExecutionStrategyImpl">
    <runSequentially>false</runSequentially>
  </executionStrategy>
</matrix-project>
"""

xunit_report_xml = """
<xunit plugin="xunit@1.91">
  <types>
    <QTestLibType>
      <pattern></pattern>
      <skipNoTestFiles>false</skipNoTestFiles>
      <failIfNotNew>true</failIfNotNew>
      <deleteOutputFiles>true</deleteOutputFiles>
      <stopProcessingIfError>true</stopProcessingIfError>
    </QTestLibType>
  </types>
  <thresholds>
    <org.jenkinsci.plugins.xunit.threshold.FailedThreshold>
      <unstableThreshold>0</unstableThreshold>
      <unstableNewThreshold>0</unstableNewThreshold>
      <failureThreshold>0</failureThreshold>
      <failureNewThreshold>0</failureNewThreshold>
    </org.jenkinsci.plugins.xunit.threshold.FailedThreshold>
    <org.jenkinsci.plugins.xunit.threshold.SkippedThreshold>
      <unstableThreshold>0</unstableThreshold>
      <unstableNewThreshold>0</unstableNewThreshold>
      <failureThreshold>0</failureThreshold>
      <failureNewThreshold>0</failureNewThreshold>
    </org.jenkinsci.plugins.xunit.threshold.SkippedThreshold>
  </thresholds>
  <thresholdMode>1</thresholdMode>
  <extraConfiguration>
    <testTimeMargin>3000</testTimeMargin>
  </extraConfiguration>
</xunit>
"""

build_preamble_template = """
if [ -e conf ]; then
    rm -rf conf
fi

mkdir conf
cp $HOME/test-utils/testrunner.py conf
{{#multijob}}
cat > conf/${{AXIS_NAME}}-{{name}}.sh << EOF
{{/multijob}}
{{^multijob}}
cat > conf/{{name}}.sh << EOF
{{/multijob}}
sed -i -e 's#archive.ubuntu.com#10.0.15.179:3142\/us.archive.ubuntu.com#' /etc/apt/sources.list
apt-get update
"""

build_packages_template = """
apt-get -q -y install {{packages}}
"""

build_configuration_dependent_template = """
case \${{AXIS_NAME}} in
{{cases}}
esac
"""

build_compile_template = """
{{#multijob}}
cd /mnt/\${{AXIS_NAME}}
{{/multijob}}
{{^multijob}}
cd /mnt/{{name}}
{{/multijob}}

if [ -e build ]; then rm -rf build; fi

{{build-steps}}
"""

build_test_template = """
{{test-steps}}
"""

# chown source dirs back to uid 1000, since jenkins-slave's jenkins has uid 1000.
build_docker_template = """
{{#multijob}}
cd /mnt/\${{AXIS_NAME}}
chown -R 1000:1000 .
{{/multijob}}
{{^multijob}}
cd /mnt/{{name}}
chown -R 1000:1000 .
{{/multijob}}
EOF

{{#multijob}}
chmod +x conf/${{AXIS_NAME}}-{{name}}.sh
docker run --rm --net host \\
        -e "{{AXIS_NAME}}=${{AXIS_NAME}}" \\
        -v $PWD:/mnt/${{AXIS_NAME}}:rw \\
        -v /home/jenkins/.ssh:/root/.ssh:ro \\
        {{IMAGE}} /bin/bash /mnt/${{AXIS_NAME}}/conf/${{AXIS_NAME}}-{{name}}.sh
{{/multijob}}
{{^multijob}}
chmod +x conf/{{name}}.sh
docker run --rm --net host \\
        -v $PWD:/mnt/{{name}}:rw \\
        -v /home/jenkins/.ssh:/root/.ssh:ro \\
        {{IMAGE}} /bin/bash /mnt/{{name}}/conf/{{name}}.sh
{{/multijob}}
"""


class HighballManifest(object):
    def __init__(self, job_file, warnings=True,
                                 url=None,
                                 username=None,
                                 password=None,
                                 new_job=False,
                                 debug=False):
        self.job_file = open(job_file, 'r')
        self.warnings = warnings

        self.url = url
        self.username = username
        self.password = password

        self.debug = debug

        self.new_job = new_job

        # Is this job a multi-job? Determined in validate_configuration.
        self.multijob = False

        # Tied to self.multijob. Name of the axis for the multi-job.
        self.axisName = ""

        # Enable test functionality.
        self.tests = False

        self.image = "ubuntu:14.04"

        # Override pystache's default escape behavior.
        pystache.defaults.TAG_ESCAPE = lambda u: escape(u, quote=False)

    def _warning(self, warning_message):
        if self.warnings:
            print >> sys.stderr, "warning: %s" % (warning_message)

    def _error(self, error_message):
        print >> sys.stderr, "error: %s" % (error_message)
        exit(1)

    def _parse_yaml(self):
        try:
            yaml = load(self.job_file, Loader=Loader)
        except YAMLError as e:
            self._error("failed to parse (%s): %s" % (self.job_file.name,
                                                      str(e)))
        else:
            return yaml

    def _validate_type(self, yaml):
        if 'type' not in yaml:
            self._error("missing config type. specify: type: <software | basic>")

        if yaml['type'] != 'software' and yaml['type'] != 'basic':
            self._error("unknown config type. supported types: software, basic")

    def _validate_git(self, yaml):
        if "git" in yaml:
            if "url" not in yaml["git"]:
                self._error("missing git repository subsection, \"url:\".")

            if "branch" not in yaml["git"]:
                self._warning("missing git repository subsection, \"branch:\". defaulting to master.")
                yaml["git"]["branch"] = "master"
        else:
            self._warning("missing top-level section, \"git:\"")

    def _validate_configurations(self, yaml):
        if "configurations" in yaml:
            configs = copy.deepcopy(yaml["configurations"])

            if "global" in configs:
                if "packages" in configs["global"]:
                    del configs["global"]
                else:
                    self._error("missing global package subsection, \"packages:\".")

                if len(configs) > 0:
                    self.multijob = True
                    axisDefined = False
                    if "name" in configs:
                        axisDefined = True
                        self.axisName = configs["name"]

                    del configs["name"]

                    if not axisDefined:
                        self._error("alternative configurations specified without assigning a name")

                    if len(configs) > 0:
                        for config_name in configs:
                            if "packages" not in configs[config_name]:
                                self._error("no packages specified for alternate configuration")
                    else:
                        self._error("name assigned for alternative configurations, without providing values")
            else:
                self._warning("missing global configuration subsection, \"global:\".")
        else:
            self._warning("missing configurations section, \"configurations:\"")

    def _validate_tests(self, yaml):
        if "test-steps" not in yaml:
            self._warning("missing test steps section, \"test-steps:\"")
        else:
            self.tests = True

        if self.tests and ("test-file-pattern" not in yaml):
            self._error("missing test file pattern section, \"test-file-pattern:\"")

        if self.tests and ("test-failure-threshold" not in yaml):
            self._warning("missing test-failure-threshold, defaulting to 0")
            yaml['test-failure-threshold'] = 0

        if self.tests and ("test-skipped-threshold" not in yaml):
            self._warning("missing test-skipped-threshold, defaulting to 0")
            yaml['test-skipped-threshold'] = 0

    def _validate_configuration(self, yaml):
        if "name" not in yaml:
            self._error("missing job name tag, \"name:\", from top-level.")

        self._validate_type(yaml)
        self._validate_git(yaml)
        self._validate_configurations(yaml)
        self._validate_tests(yaml)

        if "build-steps" not in yaml:
            self._warning("missing build steps section, \"build-steps:\"")

    def _escape_shell_variables(self, steps):
        # FIXME Make sure we don't escape parameters.
        return steps.replace("$", "\\$")

    def _generate_preamble_closing(self, yaml_dump):
        preamble = pystache.render(build_preamble_template, yaml_dump)
        closing = pystache.render(build_docker_template, yaml_dump)
        return (preamble, closing)

    def _generate_global_packages(self, yaml_dump):
        packages = ""
        if "global" in yaml_dump["configurations"]:
            yaml_dump["configurations"]["global"]["packages"] = ' '.join(yaml_dump["configurations"]["global"]["packages"])
            packages = pystache.render(build_packages_template, yaml_dump["configurations"]["global"])
        return packages

    def _generate_config_dep_packages(self, yaml_dump):
        config_dependent_packages = ""
        cases = []
        case_statement = "    {{value}})\n        apt-get install -q -y {{packages}};;"
        if self.multijob:
            configs = copy.deepcopy(yaml_dump["configurations"])
            del configs["global"]
            del configs["name"]
            for config in configs:
                # No packages to install for multi-job.
                if configs[config]["packages"] == "None":
                    continue
                case = {}
                case['value'] = config
                case['packages'] = ' '.join(configs[config]["packages"])
                cases.append(pystache.render(case_statement, case))

            # Only generate special multi-job installs if we have packages to install.
            if cases:
                config_dependent_packages = pystache.render(build_configuration_dependent_template,
                                                            {'cases': '\n'.join(cases), 'AXIS_NAME': self.axisName})
        return config_dependent_packages

    def _generate_build_steps(self, yaml_dump):
        build_steps = ""
        if "build-steps" in yaml_dump:
            yaml_dump["build-steps"] = self._escape_shell_variables(yaml_dump["build-steps"])
            build_steps = pystache.render(build_compile_template, yaml_dump)
        return build_steps

    def _generate_test_steps(self, yaml_dump):
        test_steps = ""
        if "test-steps" in yaml_dump:
            yaml_dump["test-steps"] = self._escape_shell_variables(yaml_dump["test-steps"])
            test_steps = pystache.render(build_test_template, yaml_dump)
        return test_steps

    def _generate_software_build(self, yaml):
        self._validate_configuration(yaml)
        yaml["AXIS_NAME"] = self.axisName
        yaml["multijob"] = self.multijob
        yaml["IMAGE"] = self.image

        (preamble, closing) = self._generate_preamble_closing(yaml)
        packages = self._generate_global_packages(yaml)
        config_dependent_packages = self._generate_config_dep_packages(yaml)
        build_steps = self._generate_build_steps(yaml)
        test_steps = self._generate_test_steps(yaml)

        return (preamble + packages + config_dependent_packages +
                    build_steps + test_steps + closing)

    def _generate_basic_build(self, yaml):
        if 'build-steps' in yaml:
            return yaml['build-steps']
        else:
            return ''


    def _generate_build_script(self, yaml):
        self._validate_configuration(yaml)
        if yaml['type'] == 'software':
            return self._generate_software_build(yaml)
        elif yaml['type'] == 'basic':
            return self._generate_basic_build(yaml)

    def _configure_parameters(self, xml_config, yaml):
        def stringParameter(name, description, default=''):
            stringParamDefinition = ET.Element('hudson.model.StringParameterDefinition')

            paramName = ET.SubElement(stringParamDefinition, 'name')
            paramName.text = name

            paramDescription = ET.SubElement(stringParamDefinition, 'description')
            paramDescription.text = description

            defaultValue = ET.SubElement(stringParamDefinition, 'defaultValue')
            defaultValue.text = default

            return stringParamDefinition

        def booleanParameter(name, description, default=True):
            booleanParamDefinition = ET.Element('hudson.model.BooleanParameterDefinition')

            paramName = ET.SubElement(booleanParamDefinition, 'name')
            paramName.text = name

            paramDescription = ET.SubElement(booleanParamDefinition, 'description')
            paramDescription.text = description

            defaultValue = ET.SubElement(booleanParamDefinition, 'defaultValue')
            defaultValue.text = str(default).lower()

            return booleanParamDefinition

        def choiceParameter(name, description, values=[]):
            choiceParamDefinition = ET.Element('hudson.model.ChoiceParameterDefinition')

            paramName = ET.SubElement(choiceParamDefinition, 'name')
            paramName.text = name

            paramDescription = ET.SubElement(choiceParamDefinition, 'description')
            paramDescription.text = description

            choices = ET.SubElement(choiceParamDefinition, 'choices')
            a = ET.SubElement(choices, 'a')
            a.attrib['class'] = 'java.util.Arrays$ArrayList'
            for value in values:
                string = ET.SubElement(a, 'string')
                string.text = value

            return choiceParamDefinition

        def textParameter(name, description, default=''):
            textParamDefinition = ET.Element('hudson.model.TextParameterDefinition')

            paramName = ET.SubElement(textParamDefinition, 'name')
            paramName.text = name

            paramDescription = ET.SubElement(textParamDefinition, 'description')
            paramDescription.text = description

            defaultValue = ET.SubElement(textParamDefinition, 'defaultValue')
            defaultValue.text = default

            return textParamDefinition

        properties = xml_config.find('properties')
        if properties is not None:
            for node in properties.getchildren():
                properties.remove(node)
        else:
            properties = ET.SubElement(xml_config, 'properties')

        hudsonModel = ET.SubElement(properties, 'hudson.model.ParametersDefinitionProperty')
        paramDef = ET.SubElement(hudsonModel, 'parameterDefinitions')

        parameterFunctions = {'boolean': booleanParameter,
                              'string': stringParameter,
                              'choice': choiceParameter,
                              'text': textParameter}
        for parameter in yaml['parameters']:
            type = yaml['parameters'][parameter]['type']
            default = yaml['parameters'][parameter]['default']
            description = yaml['parameters'][parameter]['description']
            paramDef.append(parameterFunctions[type](parameter, description, default))

    def _configure_repository(self, xml_config, yaml):
        """Configure SCM. Currently, we only configure git repositories."""
        scm = xml_config.find('scm')
        if scm is not None:
            for node in scm.getchildren():
                scm.remove(node)
        else:
            scm = ET.SubElement(xml_config, 'scm')

        scm.attrib['class']  = 'hudson.plugins.git.GitSCM'
        scm.attrib['plugin'] = 'git@2.2.7'

        configVersion = ET.SubElement(scm, 'configVersion')
        configVersion.text = '2'

        userRemoteConfigs = ET.SubElement(scm, 'userRemoteConfigs')
        hudsonRemoteConfig = ET.SubElement(userRemoteConfigs, 'hudson.plugins.git.UserRemoteConfig')
        url = ET.SubElement(hudsonRemoteConfig, 'url')
        url.text = yaml['git']['url']

        branches = ET.SubElement(scm, 'branches')
        branchSpec = ET.SubElement(branches, 'hudson.plugins.git.BranchSpec')

        name = ET.SubElement(branchSpec, 'name')
        name.text = yaml['git']['branch']

        generateSubmodule = ET.SubElement(scm, 'doGenerateSubmoduleConfigurations')
        generateSubmodule.text = 'false'

        submoduleCfg = ET.SubElement(scm, 'submoduleCfg')
        submoduleCfg.attrib['class'] = 'list'

        ET.SubElement(scm, 'extensions')

    def _configure_triggers(self, xml_config):
        """Configure triggers for polling SCM. Defaults to every 5 minutes."""
        triggers = xml_config.find('triggers')
        if triggers is not None:
            for node in triggers.getchildren():
                triggers.remove(node)
        else:
            triggers = ET.SubElement(xml_config, 'triggers')

        scmTrigger = ET.SubElement(triggers, 'hudson.triggers.SCMTrigger')
        spec = ET.SubElement(scmTrigger, 'spec')
        spec.text = 'H/5 * * * *'

        postCommitHooks = ET.SubElement(scmTrigger, 'ignorePostCommitHooks')
        postCommitHooks.text = 'false'

    def _configure_matrix(self, xml_config, yaml):
        textAxis = xml_config.find('axes/hudson.matrix.TextAxis')
        if textAxis is not None:
            for name in textAxis.findall(".//name"):
                textAxis.remove(name)
            for value in textAxis.findall(".//values"):
                textAxis.remove(value)

        name = ET.SubElement(textAxis, 'name')
        name.text = self.axisName
        values = ET.SubElement(textAxis, 'values')
        for axisValue in yaml['configurations']:
            if (axisValue == "name") or (axisValue == "global"):
                continue
            sub = ET.SubElement(values, 'string')
            sub.text = axisValue

    def _configure_build_command(self, xml_config, build_script):
        builders = xml_config.find('builders')
        if builders is not None:
            for node in builders.getchildren():
                builders.remove(node)
        else:
            print "builders is None"
            builders = ET.SubElement(xml_config, 'builders')

        hudsonShell = ET.SubElement(builders, 'hudson.tasks.Shell')
        command = ET.SubElement(hudsonShell, 'command')
        command.text = build_script

    def _configure_test_reports(self, xml_config, file_pattern, thresholds):
        """Operates on the 'publishers' section of config.xml. This sets the
           test report file pattern to look for and, also, the thresholds for failure and
           skipped tests.

           xml_config: current config.xml
           file_pattern: file pattern to use to find test reports
           thresholds: a dictionary containing "failure", "skipped, and their corresponding threshold values, 0 - n

           FIXME
        """
        xunit = ET.fromstring(xunit_report_xml)
        publishers = xml_config.find('publishers')
        if publishers is not None:
            for xunit in publishers.findall(".//xunit"):
                publishers.remove(xunit)
        else:
            publishers = ET.SubElement(xml_config, 'publishers')

        pattern = xunit.find("types/QTestLibType/pattern")
        pattern.text = file_pattern

        failure = xunit.find("thresholds/org.jenkinsci.plugins.xunit.threshold.FailedThreshold")
        unstableThresholdFailed = failure.find("unstableThreshold")
        unstableThresholdFailed.text = thresholds["failure"]
        unstableNewThresholdFailed = failure.find("unstableNewThreshold")
        unstableNewThresholdFailed.text = thresholds["failure"]
        failureThresholdFailed = failure.find("failureThreshold")
        failureThresholdFailed.text = thresholds["failure"]
        failureNewThresholdFailed = failure.find("failureNewThreshold")
        failureNewThresholdFailed.text = thresholds["failure"]

        skipped = xunit.find("thresholds/org.jenkinsci.plugins.xunit.threshold.SkippedThreshold")
        unstableThresholdSkipped = skipped.find("unstableThreshold")
        unstableThresholdSkipped.text = thresholds["skipped"]
        unstableNewThresholdSkipped = skipped.find("unstableNewThreshold")
        unstableNewThresholdSkipped.text = thresholds["skipped"]
        failureThresholdSkipped = skipped.find("failureThreshold")
        failureThresholdSkipped.text = thresholds["skipped"]
        failureNewThresholdSkipped = skipped.find("failureNewThreshold")
        failureNewThresholdSkipped.text = thresholds["skipped"]

        publishers.append(xunit)

    def update_job_build(self):
        """Updates/Creates a Jenkins job."""

        # Parse the YAML job config.
        yaml = self._parse_yaml()

        # Generate the shell script to be run for the build.
        build_script = self._generate_build_script(yaml)

        # Something went really wrong, we have no build script.
        if not build_script:
            self._error("An unknown error occured during build script generation. Exiting.")

        if self.url:
            highball = jenkins.Jenkins(self.url, self.username, self.password)

        job_config = None
        if self.new_job or self.debug:
            if self.multijob:
                job_config = matrix_job_xml
            else:
                job_config = single_job_xml
        else:
            job_config = highball.get_job_config(yaml["name"])

        if not job_config:
            self._error("unable to update job config for %s from Jenkins server" % yaml["name"])

        xml_config = ET.fromstring(job_config)

        if 'git' in yaml:
            self._configure_repository(xml_config, yaml)
            self._configure_triggers(xml_config)

        if self.multijob:
            self._configure_matrix(xml_config, yaml)

        if 'parameters' in yaml:
            self._configure_parameters(xml_config, yaml)

        self._configure_build_command(xml_config, build_script)

        if self.tests:
            thresholds = {}
            thresholds["failure"] = str(yaml["test-failure-threshold"])
            thresholds["skipped"] = str(yaml["test-skipped-threshold"])
            self._configure_test_reports(xml_config, yaml["test-file-pattern"], thresholds)

        if self.debug:
            print ET.tostring(xml_config)
            return

        try:
            if self.new_job:
                highball.create_job(yaml["name"], ET.tostring(xml_config))
            else:
                highball.reconfig_job(yaml["name"], ET.tostring(xml_config))
        except Exception as e:
            self._error(e)


def parse_args():
    parser = argparse.ArgumentParser(description="Job script generator for Jenkins.")
    parser.add_argument("path", type=str, help="YAML job description")
    parser.add_argument("-w", "--warnings", action="store_false", default=True,
                        help="disable warning messages during build script generation.")
    parser.add_argument("-d", "--debug", action="store_true", default=False,
                        help="Only print generated job script.")
    parser.add_argument("-ip", "--ip", action="store", default=None,
                        help="IP address of the Jenkins server.")
    parser.add_argument("-u", "--username", action="store", default=None,
                        help="Jenkins API username.")
    parser.add_argument("-p", "--password", action="store", default=None,
                        help="Jenkins API password.")
    parser.add_argument("-n", "--new-job", action="store_true", default=False,
                        help="Create a new Jenkins job.")
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    manifest = HighballManifest(args.path,
                                args.warnings,
                                args.ip,
                                args.username,
                                args.password,
                                args.new_job,
                                args.debug)
    manifest.update_job_build()
