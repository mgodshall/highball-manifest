# highball-manifest

Highball-manifest is a tool that creates Jenkins jobs, which run in a clean
Docker Ubuntu container, from YAML config files. 

## Install

To use highball-manifest you must first install the following dependencies:

    sudo pip install python-jenkins
    sudo pip install pystache
    sudo apt-get install python-yaml

Then:

    chmod +x src/highball-manifest.py
    cp src/highball-manifest.py ~/bin/highball-manifest

or wherever you want to install it.

## Usage

To update a current job:

    highball-manifest -ip <Jenkins URL> <YAML configuration file>

To create a new job:

    highball-manifest -ip <Jenkins URL> -n <YAML configuration file>

If authentication is required:

    highball-manifest -ip <Jenkins URL> -u <username> -p <password> <YAML configuration file>

Full list of command line options:

    usage: highball_manifest.py [-h] [-w] [-d] [-ip IP] [-u USERNAME]
                                [-p PASSWORD] [-n]
                                path
    
    Job script generator for Jenkins.
    
    positional arguments:
      path                  YAML job description
    
    optional arguments:
      -h, --help            show this help message and exit
      -w, --warnings        disable warning messages during build script
                            generation.
      -d, --debug           Only print generated job script.
      -ip IP, --ip IP       IP address of the Jenkins server.
      -u USERNAME, --username USERNAME
                            Jenkins API username.
      -p PASSWORD, --password PASSWORD
                            Jenkins API password.
      -n, --new-job         Create a new Jenkins job.


## YAML Job Config File Format

### Name
The name line is required across config formats.

    name: <project name>

### Type
The type of job to generate. Currently, `software` and `basic` are supported.

    type: <software | basic>

#### software
The `software` config type handles pulling in packages you specify in
`configurations` and launching a Docker container to perform the steps listed
in the `build-steps` and `test-steps` sections.

#### basic
The `basic` config type only performs the steps listed in `build-steps` sans
package management and Docker container of the `software` type.

### Git
The `git` section and `url` subsection are required. If the `branch` subsection
is not specified, then the default branch is master.

    git:
        url: <git repository url>
        branch: <git branch to check>

### Configurations
The `configurations` section is where you specify packages to be installed, axis
name, and alternate configurations. The global section contains packages that
always get installed, regardless of configuration. If you're creating a single
configuration job, this will be the only section you need to specify for
packages to install.

Packages are specified in list form, with a hyphen at the beginning of each line.

All configurations are optional, including `global`.

#### Example

    configurations:
        global:
            packages:
                - build-essential
                - python

Multi-configuration jobs can also be specified. Each named configuration will
be its own sub-build in the Jenkins job.  This uses the Jenkins matrix project
job feature.

#### Example

    configurations:
        global:
            packages:
                - build-essential
                - python

        name: QT_SELECT
        qt4:
            packages:
                - qt4-default
        
        qt5:
            packages:
                - qt5-default

The `name` field sets a variable name to refer to the configurations. Using the
example, you can refer to the current configuration using `$QT_SELECT` in the
`build-steps` and `test-steps` section. `qt4` and `qt5` will be the possible
values that `$QT_SELECT` can have.

Under each alternative configuration name section, a `packages` subsection is
needed. The packages are specified the same way as in the `global` package
section above.

### Build Steps
The `build-steps` section is a list of bash commands to perform to build the job.

#### Example

    build-steps: |
        mkdir build
        cd build
        qmake ..
        make
        make install

### Test Steps
The `test-steps` section is a list of bash commands to perform after building
to run unit tests included with the project.  If `test-steps` is specified,
`test-file-pattern` must also be specified. This is either a glob pattern or
path to a QTestLib XML file output by the tests for use in the XUnit test
report.

#### Example

    test-steps: |
        ../conf/testrunner.py -r unittests/

    test-file-pattern: build/*.xml

In addition to the test parameters above, you can optionally specify the
flagging of a build as unstable or failing with skipped
(`test-skipped-threshold`) and failed (`test-failure-threshold`) thresholds.
If the thresholds are not specified, both values default to `0`.

#### Example

    test-skipped-threshold: 1
    test-failure-threshold: 0

